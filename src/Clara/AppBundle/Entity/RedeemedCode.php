<?php

namespace Clara\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RedeemedCode
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class RedeemedCode
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}