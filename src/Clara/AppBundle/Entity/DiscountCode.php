<?php

namespace Clara\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * DiscountCode
 *
 * @ORM\Table(name="clara_discount_code")
 * @ORM\Entity(repositoryClass="DiscountCodeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class DiscountCode
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $code;
    
    /**
     *
     * @ORM\Column(type="boolean", options={"default" = 0} )
     */
    protected $redeemed;
    
    /**
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $redeemed_by_registrant_id;
    
    /**
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $type;
    
    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    public function __construct()
    {
        $this->redeemed = FALSE;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return DiscountCode
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set redeemed
     *
     * @param boolean $redeemed
     * @return DiscountCode
     */
    public function setRedeemed($redeemed)
    {
        $this->redeemed = $redeemed;
    
        return $this;
    }

    /**
     * Get redeemed
     *
     * @return boolean 
     */
    public function getRedeemed()
    {
        return $this->redeemed;
    }

    /**
     * Set redeemed_by_registrant_id
     *
     * @param string $redeemedByRegistrantId
     * @return DiscountCode
     */
    public function setRedeemedByRegistrantId($redeemedByRegistrantId)
    {
        $this->redeemed_by_registrant_id = $redeemedByRegistrantId;
    
        return $this;
    }

    /**
     * Get redeemed_by_registrant_id
     *
     * @return string 
     */
    public function getRedeemedByRegistrantId()
    {
        return $this->redeemed_by_registrant_id;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return DiscountCode
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return DiscountCode
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return DiscountCode
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
}