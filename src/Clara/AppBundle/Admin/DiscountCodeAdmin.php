<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Clara\AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Description of DiscountCodeAdmin
 *
 * @author leonardo
 */
class DiscountCodeAdmin extends Admin
{
    public function configureFormFields(FormMapper $form)
    {
        
        $form->add('code')
             ->add('type');
    }
    
    public function configureListFields(ListMapper $list)
    {
        $list->add('code')
             ->add('type')
             ->add('redeemed')
             ->add('created_at');
    }
}