<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Clara\AppBundle\Controller;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Util\Codes;
use Application\Sonata\UserBundle\Entity\User;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of RestController
 *
 * @author leonardo
 */
class RestController extends FOSRestController implements ClassResourceInterface
{
    /**
     * 
     * @Rest\Get("/info/validate-discount-code")
     * @Rest\View()
     */
    public function validateDiscountCodeAction(Request $request)
    {
        if(!$code = $request->get('code', false))
        {
            return array('msn' => 'ERROR', 'info' => 'Invalid code');
        }
        
        $repo         = $this->getDoctrine()->getRepository('Clara\AppBundle\Entity\DiscountCode');
        $discountCode = $repo->findOneBy(array('code' => $code, 'redeemed' => FALSE));
        
        if(is_null($discountCode))
        {
            return array('msn' => 'ERROR', 'info' => 'Invalid code');
        }
        
        return array('msn'  => 'OK', 
                     'info' => array('code_id'       => $discountCode->getId(), 
                                     'discount_code' => $discountCode->getCode(),
                                     'type'          => $discountCode->getType()));
    }
    
    /**
     * 
     * @Rest\Get("/info/redeem-discount-code")
     * @Rest\View()
     */
    public function redeemDiscountCodeAction(Request $request)
    {
        $codeId       = $request->get('code_id', 0);
        $discountCode = $request->get('discount_code', 0);
        $requesterInf = $request->get('requester_inf');
        $repo         = $this->getDoctrine()->getRepository('Clara\AppBundle\Entity\DiscountCode');
        $code         = $repo->findOneBy(array('id' => $codeId, 'code' => $discountCode, 'redeemed' => FALSE));
        
        if(is_null($code))
        {
            return array('msn' => 'ERROR', 'info' => 'Discount code not found');
        }
        
        $code->setRedeemed(TRUE);
        $code->setRedeemedByRegistrantId($requesterInf);
        
        $em = $this->getDoctrine()->getManager();
        $em->flush($code);
        
        return array('msn' => 'OK', 'info' => '');        
    }
}
