<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Clara\AppBundle\Controller;

use Lumen\SonataExtendsBundle\Controller\CustomAdminController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of DiscountCodeController
 *
 * @author leonardo
 */
class DiscountCodeController extends Controller
{
    /**
     * 
     * @Route("/admin/discount-codes", name="discount_codes")
     * @Template()
     */
    public function indexAction()
    {
        return $this->render('ClaraAppBundle:DiscountCode:index.html.twig');
    }
    
    /**
     * @Route("/admin/generate-discount-codes", name="generate_discount_codes")
     * @Template()
     * @param \Clara\AppBundle\Controller\Request $request
     */
    public function generateDiscountCodesAction(Request $request)
    {
        if($request->isMethod('POST'))
        {
            $quantity  = $request->get('quantity', 1);
            $type      = $request->get('type');
            $generator = $request->get('generator_type');
            $result    = $this->generateDiscountCodes($quantity, $type);
            
            if(!$result)
                return $this->renderJson(array('msn' => 'ERROR', 'info' => 'Imposible generar más códigos de 4 digitos'));
                       
            return $this->renderJson(array('msn' => 'OK'));
        }
        
        return $this->renderJson(array('ERROR' => 404), 404);
    }
    
    public function generateDiscountCodes($cantidad, $type)
    {
        $generados = 0;
        $em        = $this->getDoctrine()->getManager();
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        for ($index = 0; $index < $cantidad; $index++)
        {
            $rawCode = $this->gen_uuid();
            $_codigo = $rawCode;
            $intents = 0;
            while ($this->checkCodeInTable($_codigo))
            {
                $intents ++;
                $rawCode = $this->gen_uuid();
                $_codigo = $rawCode;
                
                if($intents >= 10)
                    return false;
            }
            $codigo = new \Clara\AppBundle\Entity\DiscountCode();
            $codigo->setCode($_codigo);
            $codigo->setType($type);
            $em->persist($codigo);
            $em->flush();
            $generados ++;
        }
        
        return $generados;
    }
    
    private function checkCodeInTable($code)
    {
        $em     = $this->getDoctrine()->getManager();
        $repo   = $em->getRepository('ClaraAppBundle:DiscountCode');
        $codigo = $repo->findOneBy(array('code' => $code));
        
        return ($codigo) ? true : false;                
    }
    
    private function gen_uuid($len = 4)
    {
        $hex  = md5("flkdasjfl87987kljdf9090876asdf7" . uniqid("", true));
        $pack = pack('H*', $hex);
        $tmp  = base64_encode($pack);
        $uid  = preg_replace("#(*UTF8)[^A-Za-z0-9]#", "", $tmp);
        $len  = max(4, min(128, $len));

        while (strlen($uid) < $len)
            $uid .= gen_uuid(22);

        return substr($uid, 0, $len);
    }
}
