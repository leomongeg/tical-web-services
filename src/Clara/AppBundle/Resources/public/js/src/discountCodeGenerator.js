$(function() {
    if ($('#codigo_cantidad').length > 0)
    {
        console.log('DISCOUNT_CODE_GENERATOR');
        
        $('#btn_generate_code').click(generateDiscountCodes);
    }
});

function generateDiscountCodes()
{
    var $sender  = $(this);
    var quantity = $('#codigo_cantidad').val();
    
    if(!isNumber(quantity))
    {
        alert('El valor ingresado es invalido.');
        return;
    }
    
    var type = $('#code_type').val();
    
    if(type.trim() === '')
    {
        alert('El tipo es requerido');
        return;
    }
    
    $('#loader-container1').show('slow');
    $sender.unbind('click');
    $.ajax({
        type    : "POST",
        url     : '/admin/generate-discount-codes',
        dataType: 'json',
        data    : {
            generator_type: "GENERATE_ONLY",
            quantity      : quantity,
            type          : type
        },
        success: function(response) {
            $('#loader-container1').hide('slow');
            $sender.click(generateDiscountCodes);
            if (response.msn === 'ERROR')
            {
                alert(response.info);
                return;
            }
        }
    });
}

/**
 * Verifica si n es numerico o string
 * 
 * @param {type} n
 * @returns {bool|Boolean}
 */
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}