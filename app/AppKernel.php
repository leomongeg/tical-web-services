<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new Sonata\CoreBundle\SonataCoreBundle(),
            new Sonata\BlockBundle\SonataBlockBundle(),
            new Sonata\jQueryBundle\SonatajQueryBundle(),
            new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
            new Sonata\AdminBundle\SonataAdminBundle(),
            new Sonata\UserBundle\SonataUserBundle('FOSUserBundle'),
            new Knp\Bundle\MarkdownBundle\KnpMarkdownBundle(),
            new Sonata\MarkItUpBundle\SonataMarkItUpBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new FM\ElfinderBundle\FMElfinderBundle(),
            new Sonata\IntlBundle\SonataIntlBundle(),
            new Application\Sonata\UserBundle\ApplicationSonataUserBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Oh\GoogleMapFormTypeBundle\OhGoogleMapFormTypeBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Lumen\AppBundle\LumenAppBundle(),
            /**
             * Bundles opcionales muy utiles.
             */
            
            new Hpatoio\DeployBundle\DeployBundle(), // Para deployments mediante rsync.
//            new A2lix\TranslationFormBundle\A2lixTranslationFormBundle(), // Para formularios de traducciones de sonata.
//            new Prezent\Doctrine\TranslatableBundle\PrezentDoctrineTranslatableBundle(), // Para formularios de traducciones de sonata mejor que el anterior.
//            new FOS\JsRoutingBundle\FOSJsRoutingBundle(), // Mapping de las rutas para javascript, muy conveniente cuanto la app esta basada en ajax y se requiere generar las urls para dichas llamadas.
//            new Bazinga\Bundle\JsTranslationBundle\BazingaJsTranslationBundle(), // Mapping de las traducciones a javascript, muy util si la app esta basada en javascript y es requerido hacer validaciones y mostrar los errores a los usuarios en su idioma.
//            new Liip\ImagineBundle\LiipImagineBundle(), // Redimensionado de imagenes.
            new FOS\RestBundle\FOSRestBundle(), //FOS Rest Bundle para web services
            new Escape\WSSEAuthenticationBundle\EscapeWSSEAuthenticationBundle(), // Para implementar autenticación de usuarios mediante WSSE
            new Nelmio\CorsBundle\NelmioCorsBundle(), // Para evitar el problema de los CORS cuando se accede a los web services desde otro dominio o si se utilizar Phonegap o Angular.
            new Lumen\SonataExtendsBundle\LumenSonataExtendsBundle(),
            new Clara\AppBundle\ClaraAppBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Acme\DemoBundle\AcmeDemoBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
    
    public function init()
    {
        date_default_timezone_set('America/Costa_Rica');
        parent::init();
    }
}
